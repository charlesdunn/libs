img_dir = '../INRIAPerson/Test/pos/'; % path to the pos example images

gt_dir  = 'gt_bbox/';                    % comes with INRIA dataset and 
                                      % converted to txt format

% non max supresession params
nmax_param.sw = 0.1;                  %bandwidth along width
nmax_param.sh = 0.1;                  %bandwidth along height 
nmax_param.ss = 1.3;                  %bandwidth along scale 


clear det; clear fppi;        

%Dalal and Triggs (CVPR'05, linear SVM + HOG)
det_dir = 'dt_hog/stride_8_8_sqrt_sqrt_sqrt_2/';
[det{1},fppi{1}]=benchmark_det_inria(det_dir,img_dir,gt_dir);

det_dir = 'dt_hog/stride_8_8_1.05/';
[det{2},fppi{2}]=benchmark_det_inria(det_dir,img_dir,gt_dir);


%model1 
nmax_param.th = -0.5;
det_dir = 'model1/';
[det{3},fppi{3}]=benchmark_det(det_dir,img_dir,gt_dir,nmax_param);

%model2 
nmax_param.th = 0;
det_dir = 'model2/';
[det{4},fppi{4}]=benchmark_det(det_dir,img_dir,gt_dir,nmax_param);

legtxt={'8x8, 1.0905, dalal-triggs';
        '8x8, 1.0500, dalal-triggs';
        '8x8, 1.0905, iksvm(model1)';
        '8x8, 1.0905, iksvm(model2)'};
    


h=figure;
hold on;
sty = {'--k';'-k';'-r';'-b';'-m'};
for i = 1:length(det)
     plot(fppi{i},det{i},sty{i},'LineWidth',2);
end;
grid on; box on;
axis([0 2 0 1]);
xlabel('False Pos Per Image');
ylabel('Detection Rate');
legend(legtxt,'Location','SouthEast');
title('INRIAPerson Dataset Detection Plots');
