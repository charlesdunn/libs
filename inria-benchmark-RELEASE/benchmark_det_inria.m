function [det,fppi]=benchmark_det_inria(det_dir,img_dir,gt_dir)
  DISPLAY=0;
  files = dir([det_dir 'det+pos/*.mat']);

  th = 0.0; %better than -0.1 and +0.1 510/589 detections

  %border fractions
  bw=16/64; bh=16/128;

  pos_scores=[];
  pos_labels=[];
  neg_scores=[];
  neg_labels=[];
  npos = 0;
  toti = length(files);
  %loop over images
  fprintf(1,'+images[%i]:',length(files));

  for i = 1:length(files)
    if(mod(i,20)==0), fprintf(1,'%i..',i); end;
    matf = files(i).name;
    gtf  = [gt_dir matf(1:end-3) 'txt'];
    detf = [det_dir 'det+pos/' matf];    
    
    gt = load(gtf); %ground truth
    tmp = load(detf); % raw detections
    dr = tmp.aa.dr;
    ds = tmp.aa.ds;

    
    %remove borders
    dr(:,1) = dr(:,1) + bw*dr(:,3);
    dr(:,2) = dr(:,2) + bh*dr(:,4);
    dr(:,3) = (1-2*bw)*dr(:,3);
    dr(:,4) = (1-2*bh)*dr(:,4);

    %correct gt to x,y,w,h format
    gt(:,3) = gt(:,3) - gt(:,1);
    gt(:,4) = gt(:,4) - gt(:,2);

    %assign labels to bboxes
    l = assign_gt_bbox(gt,dr,ds);

    %update scores
    pos_scores = [pos_scores; ds];
    pos_labels = [pos_labels; l];
    npos = npos + size(gt,1);

    if(DISPLAY)
      imgf = [img_dir matf(1:end-3) 'png'];
      if(~exist(imgf))
        imgf = [img_dir matf(1:end-3) 'jpg'];
      end
      figure(1);clf; 
      
      subplot(2,2,2);
      draw_det(imgf, dr(:,1),dr(:,2),dr(:,3),dr(:,4),ds,th);
      title('non max supresion');
      
      subplot(2,2,3);
      s = ones(size(gt,1),1);
      draw_det(imgf,gt(:,1),gt(:,2),gt(:,3),gt(:,4),s,0);
      title('ground truth');

      subplot(2,2,4);
      draw_det(imgf, dr(:,1),dr(:,2),dr(:,3),dr(:,4),l,0);
      title('correct detections');

      %wait for user input
      [x, y, button] = ginput(1);
      %pause(0.3);
    end
  end
  %loop over the negatives and gather the scores
  files = dir([det_dir 'det+neg/*.mat']);
  toti = toti + length(files);
  fprintf(1,'[done]\n-images[%i]:',length(files));   
  for i = 1:length(files)
    if(mod(i,20)==0), fprintf(1,'%i..',i); end;
    matf = files(i).name;
    detf = [det_dir 'det+neg/' matf];    
    tmp = load(detf); % raw detections
    dr = tmp.aa.dr;
    ds = tmp.aa.ds;
    
    %all of these are false positves
    neg_scores = [neg_scores;ds];
    neg_labels = [neg_labels; zeros(size(ds))];
  end
  fprintf(1,'[done]\n')
  %plot the det_vs_fpi
  [det,fppi]=det_vs_fppi([pos_scores;neg_scores],[pos_labels;neg_labels],npos,toti);
end

%% assign detections to ground truth bboxes
function l = assign_gt_bbox(gt,dt,s)
    %compute pairwise intersection by union
    overlap = zeros(size(gt,1), size(dt,1));
    agt = gt(:,3).*gt(:,4);
    adt = dt(:,3).*dt(:,4);
    for i = 1:size(gt,1)
        for j = 1:size(dt,1)
            rr = rectint(gt(i,:),dt(j,:));
            overlap(i,j) = rr/(agt(i)+ adt(j) - rr);
        end
    end
    oT = 0.5; %PASCAL CRITERION
    notTaken = ones(1,length(s));
    l = zeros(length(s),1);
    for i = 1:size(gt,1)
        bbi = find(overlap(i,:) > oT & notTaken == 1);
        [mval,mi] = max(s(bbi));
        mindx = bbi(mi);
        notTaken(mindx) = 0;
        l(mindx) = 1;
    end
end
%% function detections/fpi
function [det,fppi]=det_vs_fppi(ss,ll,npos,toti)
    max_fpi = 2;
    th = linspace(min(ss), max(ss), 100); 
    
    det = zeros(size(th));
    fppi = zeros(size(th));
    for j = 1:length(th)
      indx = ss > th(j);
      det(j) = sum(ll(indx)==1)/npos;
      fppi(j) = sum(ll(indx)==0)/toti;
    end
    %add an endpoint for smoothness
    if(fppi(1) < max_fpi),
        det = [det(1) det ];
        fppi = [max_fpi fppi];
    end
        
end
