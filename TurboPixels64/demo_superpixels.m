% Runs the superpixel code on the lizard image

addpath('lsmlib');
load('~\Documents\MATLAB\VPD\truth\ivp_truths_1.mat');
% img = im2double(imread('lizard.jpg'));
% img = im2double(imread(imdir('Building_2.jpg')));

figure(1);
ind = 2;
imfn = ivp_truths{ind,1};
img = rgbread(imfn);
img = imresize(img,.25);
[phi,boundary,disp_img] = superpixels(img, 2000);
imagesc(disp_img);

np = numel(img)/3;

[L,num] = bwlabel(~boundary,4);
fim = zeros(size(img));
R = zeros(1,num);
G = zeros(1,num);
B = zeros(1,num);
ct = zeros(num,2);
for ii = 1:num
    reg = L==ii;
    tmp = regionprops(reg,'Centroid');
    ct(ii,:) = tmp.Centroid;
    ind = find(reg(:));
    R(ii) = mean(img(ind));
    G(ii) = mean(img(ind + np));
    B(ii) = mean(img(ind + 2*np));
    fim(ind) = R(ii);
    fim(ind + np) = G(ii);
    fim(ind + 2*np) = B(ii);
end
fim = fim + img.*repmat(boundary,[1 1 3]);
imagesc(fim);axis equal tight;

figure(2);
scatter(ct(:,1),ct(:,2),[],[R(:) G(:) B(:)],'.');
axis equal tight;
