% The make utility for all the C and MEX code

function make(command)

if (nargin > 0 && strcmp(command,'clean'))
    delete('*.mexglx');
    delete('*.mexw32');
    delete('lsmlib/*.mexglx');
    delete('lsmlib/*.mexw32');
    return;
end
mex CC=g++ DT.cpp
mex CC=g++ height_function_der.cpp
mex CC=g++ height_function_grad.cpp
mex CC=g++ local_min.cpp
mex CC=g++ zero_crossing.cpp
% mex CC=g++ -lm get_full_speed.cpp
mex CC=g++ get_full_speed.cpp

mex CC=g++ wrap.cpp
mex CC=g++ corrDn.cpp
mex CC=g++ convolve.cpp
mex CC=g++ edges.cpp
mex CC=g++ upConv.cpp wrap.cpp convolve.cpp edges.cpp

% mex CC=gcc corrDn.cpp wrap.cpp convolve.cpp edges.cpp
% mex CC=gcc upConv.cpp wrap.cpp convolve.cpp edges.cpp

cd lsmlib
mex CC=g++ computeDistanceFunction2d.cpp FMM_Core.cpp FMM_Heap.cpp lsm_FMM_field_extension2d.cpp
mex CC=g++ computeExtensionFields2d.cpp FMM_Core.cpp FMM_Heap.cpp lsm_FMM_field_extension2d.cpp
mex CC=g++ doHomotopicThinning.cpp FMM_Core.cpp FMM_Heap.cpp lsm_FMM_field_extension2d.cpp
cd ..
