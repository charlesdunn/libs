% textureTransform function
%
% author: Stanislas Larnier
% email: stanislas.larnier@laas.fr
%
% version December 20, 2014
%
% AIM
% compute the SVD, Eigen, QR and LU texture transforms
% described in Chapter 3 of A.T.Targhi's thesis, The Texture-Transform:
% An Operator for Texture Detection and Discrimination
%
% INPUTS
% windowSize: size of the window
% lowestNumber: number of considered values
% power in the formula (try 1/2, 1, 2 and 4)
% spacingParameter: to avoid computation on all the pixels
% thetaAngles: angles to rotate the window
% chosenDetection: SVD, Eigen, QR or LU
%
% OUTPUT
% outputImage: result of the texture transform

function outputImage=textureTransform(inputImage,windowSize,lowestNumber,power,spacingParameter,thetaAngles,chosenDetection,mask)

% convert in double and gray-level if necessary
if (size(inputImage,3)==3),
    M = double(rgb2gray(inputImage));
else
    M = double(inputImage);
end

if ~exist('mask','var') || isempty(mask)
    mask = ones([size(inputImage,1), size(inputImage,2)]);
end

% compute the number of raws in the output image
nbrRaws = floor(size(M,1)/spacingParameter);

% compute the number of cols in the output image
nbrCols = floor(size(M,2)/spacingParameter);

% initialize the output image
outputImage = zeros(nbrRaws,nbrCols);

% variables for the window pixel coordinates
left = -floor(windowSize/2);
right = floor((windowSize-1)/2)-1;

for i = 1:nbrRaws,
    
    for j = 1:nbrCols,
        
        if mask((i-1)*spacingParameter + 1,(j-1)*spacingParameter + 1);
        
        % window pixel coordinates
        tempi = 1+(i-1)*spacingParameter;
        indi = tempi+left:tempi+right;
        
        % change the window pixel coordinates if boundary problem
        if (min(indi) < 1), indi = 1+indi-min(indi); end
        if (max(indi) > size(M,1)), indi = indi-(max(indi)-size(M,1)); end
        
        % window pixel coordinates
        tempj = 1+(j-1)*spacingParameter;
        indj = tempj+left:tempj+right;
        
        % change the window pixel coordinates if boundary problem
        if (min(indj) < 1), indj = 1+indj-min(indj); end
        if (max(indj) > size(M,2)), indj = indj-(max(indj)-size(M,2)); end
        
        % create the window image
        windowImage = M(indi,indj);
        
        for k=1:length(thetaAngles),
            
            % rotate the image with the provide angle
            if (thetaAngles(k)~=0),
                windowImage = imrotate(windowImage,thetaAngles(k)*pi/180,'bicubic','crop');
            end
        
            % compute the singular value decomposition
            if strcmp(chosenDetection,'SVD'), vector = svd(windowImage); end

            % compute the eigen value decomposition
            if strcmp(chosenDetection,'Eigen'), vector = eig(windowImage); end
            
            % compute the orthogonal-triangular decomposition
            if strcmp(chosenDetection,'QR'), [~,R] = qr(windowImage); vector=diag(R); end

            % compute the LU matrix factorization
            if strcmp(chosenDetection,'LU'), [~,U] = lu(windowImage); vector=diag(U); end
            
            if (k==1),
                outputImage(i,j) = sum(abs(vector(lowestNumber:end)).^power);
            else
                outputImage(i,j) = min(outputImage(i,j),sum(abs(vector(lowestNumber:end)).^power));
            end
            
        end
        end
    
    end
    
end

% resize the image
outputImage = imresize(outputImage, spacingParameter, 'bicubic');