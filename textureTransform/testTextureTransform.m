% testTextureTransform
%
% author: Stanislas Larnier
% email: stanislas.larnier@laas.fr
%
% version December 20, 2014
%
% test the SVD, Eigen, QR and LU texture transforms

%% cleaning step

clc; clear all; close all;

%% Choose the texture transform

% chosenDetection = 'SVD';
% chosenDetection = 'Eigen';
% chosenDetection = 'QR';
chosenDetection = 'LU';

%% Parameters

% size of the window
windowSize = 16;

% number of considered values
lowestNumber = floor(windowSize/2);

% power in the formula (try 1/2, 1, 2 and 4)
power = 1;

% to avoid computation on all the pixels
spacingParameter = 8;

% theta angles to rotate the window
thetaAngles = [0 22.5 45 67.5];

%% Load and show input image

% inputImage = imread('Koala.jpg');
% inputImage = imread(imdir('eq/eq2.jpg'));
% inputImage = imresize(inputImage,2048/size(inputImage,2));
% inputImage = imread(imdir('city3.jpg'));

% p.fn = 'Playtable';
% 
% addpath(genpath(fullfile(homedir,'jsmos')));
% addpath(genpath(fullfile(homedir,'libs','edison_matlab_interface')));
% 
% [I1, I2] = mbread(p.fn);

% I1 = reshape(I1,[1 size(I1)]);
% I2 = reshape(I2,[1 size(I2)]);

% inputImage = cat(2,I1,I2);

inputImage = rgbread(imdir('tennis.jpg'));


figure, imshow(inputImage), pause(0.01);

%% Compute the texture transform

outputImage = textureTransform(inputImage,windowSize,lowestNumber,power,spacingParameter,thetaAngles,chosenDetection);

%% Show the result

figure, imagesc(outputImage), axis off, axis image, colormap gray,